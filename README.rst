indic-word2vec-embeddings
=========================

----

This repository contains pre-trained word embeddings of major Indian Languages. These word embeddings are trained using Skip-gram model with negative sampling which is implemented in `word2vec`_ toolkit. 

.. _`word2vec`: https://radimrehurek.com/gensim/models/word2vec.html

The repository currently has the following language embeddings:

  * Hindi 

    * Corpus size: 4,08,93,612 sentences
    * Embedding size: (701916, 64)
    * Minimum frequency: 5
    * Train iterations: 1
    * Model: skip-gram
    * Window: 1

  * Urdu

    * Corpus size: 70,35,699 sentences
    * Embedding size: (169130, 64)
    * Minimum frequency: 2
    * Train iterations: 3
    * Model: skip-gram
    * Window: 1

  * English

    * Corpus size: 20,22,15,688 sentences
    * Embedding size: (1166705, 64)
    * Minimum frequency: 20
    * Train iterations: 1
    * Model: skip-gram
    * Window: 1
